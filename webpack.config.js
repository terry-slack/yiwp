const path = require("path");
const webpack = require("webpack");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const getClientEnvironment = require("./config/env");
require("@babel/polyfill");

// Get environment variables to inject into our app.
const env = getClientEnvironment();

module.exports = {
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress: true,
    port: 9000,
    historyApiFallback: true,
    hot: false,
    publicPath: "/",
    openPage: ""
  },
  // https://webpack.js.org/configuration/devtool/
  // Need to remove eval for ie-ll
  devtool: "source-map",
  performance: {
    maxEntrypointSize: 10000,
    maxAssetSize: 10000,
    hints: false
  },
  entry: {
    index: ["@babel/polyfill", "./src/index.tsx"]
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bundle-[hash].js",
    chunkFilename: "[id][hash].js",
    publicPath: "./",
    pathinfo: false
  },
  optimization: {
    removeAvailableModules: false,
    removeEmptyChunks: false,
    splitChunks: false
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx|tsx|ts)$/,
        exclude: /(node_modules|bower_components)/,
        loader: "babel-loader"
      },
      {
        test: /\.(svg|png|jpg|gif|jpeg|ttf)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[path][name].[ext]"
            }
          }
        ]
      },
      {
        test: /\.(ts|tsx)$/,
        use: [
          {
            loader: "awesome-typescript-loader",
            options: {
              transpileOnly: true,
              experimentalWatchApi: true
            }
          },
          // Optional
          {
            loader: "react-docgen-typescript-loader"
          }
        ]
      }
    ]
  },
  stats: {
    warnings: false
  },
  resolve: {
    symlinks: false,
    cacheWithContext: false,
    extensions: [".ts", ".tsx", ".js", ".jsx"]
  },
  plugins: [
    new CleanWebpackPlugin(["dist"]),
    new HtmlWebpackPlugin({
      template: __dirname + "/public/index.html",
      lang: "en-ca",
      filename: "index.html",
      inject: true
    }),
    new UglifyJsPlugin({
      sourceMap: true
    }),
    new CopyWebpackPlugin([
      {
        from: "public/assets",
        to: "assets"
      }
    ]),
    new webpack.DefinePlugin(env.stringified)
  ],
  mode: "production"
};
