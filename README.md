## Pre-requisites

Your local machine must have the following apps installed:

`NPM, Yarn, NodeJS`

## Setup

Install NodeJS

[https://nodejs.org/en/download/]()

Install yarn

`npm install -g yarn`

Install dependencies

`yarn`

Run App

`yarn start`

Run Storybook

`yarn storybook`

## Best Practices And Naming Conventions

1.  Class names should be in PascalCase case.
2.  Component names should be camel case for readability.
3.  Group components and apps logically into folders.
4.  Self closing tags are okay.
5.  Use ES6. For a complete overview, please visit [http://es6-features.org/](http://es6-features.org/)

## Conventions/Best Practices for React Components

1.  Don't use inline styles, use React-JSS.
2.  Use functional React components as a baseline, only add in state when absoultely necessary. Functional components are easy to test and harder to break.
3.  For purely presentational components, use stateless components for quick readability.

## Best Practices for Writing Stories

1. All story files must follow this naming convention: `Component.stories.js`. Those files are automatically loaded into Storybook.
2. Wrap Components in StateManager if State is needed in Story.
3. Use Knobs to interact with component props. [https://github.com/storybooks/storybook/tree/master/addons/knobs]()
4. Use Actions to log events in Storybook. [https://github.com/storybooks/storybook/tree/master/addons/actions]()

Basic Example:

```
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import {text} from "@storybook/addon-knobs";

import { Button } from "./index";

storiesOf("Buttons", module)
  .add(
    "Button",
    () => (
      <Button onClick={action("clicked")}>
        {text("Text", "Prospective Student")}
      </Button>
    ),
    {
      notes: {
        markdown:
          "Any notes about the story or component"
      }
    }
  )
```

StateManager Example:

```
<StateManager initialState={{ show: false }}>
  {({ state, setState }) => (
    <div>
      <div className="center">
        <SelectButton
          onClick={() => setState({ show: true })}
        >
          Modal
        </SelectButton>
      </div>
      {state.show && (
        <Modal
          show={state.show}
          onModalClose={() => setState({ show: false })}
          onModalOpen={action("Modal Open")}
        >
        </Modal>
      )}
    </div>
  )}
</StateManager>
```

## Best Practices for Testing Components

Snapshots used in Storybook must be exported as JSON and imported into the config. This is done by running:

`yarn test-save": "jest --updateSnapshot --json --outputFile=./stories/.jest-test-results.json`

Test cases can be written using `enzyme` or / with `react-test-renderer` and all test file names should follow this naming convention:

`Component.test.js`.

Basic example below:

```
import { Component } from "./index";
import { shallow } from "enzyme";
import renderer from "react-test-renderer";

it("Component Renders Correctly", () => {
const tree = renderer
.create(<Component>Hello World</Component>)
.toJSON();
expect(tree).toMatchSnapshot();
});
```
