import * as React from "react";
import injectSheet from "react-jss";
import { themeListener } from "theming";

const styleSheet = (theme: any) => ({
  center: theme.center
});

interface CenterProps {
  theme?: any;
  classes?: any;
}
const Center: React.FC<CenterProps> = props => {
  return <div className={props.classes.center}>{props.children}</div>;
};

export default injectSheet(styleSheet)(Center);
