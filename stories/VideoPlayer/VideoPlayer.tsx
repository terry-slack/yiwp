import * as React from "react";
import * as PropTypes from "prop-types";
import { TweenMax } from "gsap";
import injectJss from "react-jss";
import Video from "./Players/Video";
// const icons = require("./assets/player_icons.png");

interface VideoProps {
  type: string;
  src: string;
  title?: string;
  classes?: any;
}

const styleSheet = {
  "@import":
    PropTypes.string[
      "url(https://fonts.googleapis.com/css?family=Varela+Round)"
    ],
  videoWrapper: {
    fontFamily: "Varela Round, sans-serif",
    width: "100%",
    paddingTop: "58.25%",
    height: 0,
    color: "#fff"
  },
  videoOverlay: {
    opacity: 0,
    position: "absolute",
    bottom: 0,
    width: "100%",
    height: "100%",
    left: 0,
    top: 0
  },
  videoControls: {
    bottom: 0,
    boxSizing: "border-box",
    padding: "16px",
    position: "absolute"
  }
};

const Player = (type: string, src: string) => {
  switch (type) {
    case "video":
      return <Video src={src} />;
    default:
      return <Video src={src} />;
  }
};

const onOverlayMouseEnter = (event: React.MouseEvent<HTMLDivElement>) => {
  const overlay = event.currentTarget;
  TweenMax.killDelayedCallsTo(hideControls);
  showControls(overlay);
  TweenMax.delayedCall(3, hideControls, [overlay]);
};

const onOverlayMouseLeave = (event: React.MouseEvent<HTMLDivElement>) => {
  TweenMax.to(event.currentTarget, 0.5, { opacity: 0 });
};

function showControls(controls: EventTarget) {
  TweenMax.to(controls, 0.5, { opacity: 1 });
}

function hideControls(controls: EventTarget) {
  TweenMax.to(controls, 0.5, { opacity: 0 });
}

const VideoPlayer: React.FC<VideoProps> = props => {
  React.useEffect(() => {
    console.log("Video Player Mounted");
  });

  return (
    <div className={props.classes.videoWrapper}>
      {Player(props.type, props.src)}
      <div
        className={props.classes.videoOverlay}
        onMouseEnter={onOverlayMouseEnter}
        onMouseMove={onOverlayMouseEnter}
        onMouseLeave={onOverlayMouseLeave}
      >
        <div className={props.classes.videoControls}>
          <h1>{props.title}</h1>
        </div>
      </div>
    </div>
  );
};

VideoPlayer.propTypes = {
  type: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  title: PropTypes.string
};

VideoPlayer.defaultProps = {
  type: "video"
};

export default injectJss(styleSheet)(VideoPlayer);
