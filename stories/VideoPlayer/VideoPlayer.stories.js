import React from "react";
import { storiesOf, addDecorator } from "@storybook/react";
import { text, boolean, number, select } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import VideoPlayer from "./VideoPlayer.tsx";
import { StateManager, Center } from "../Helpers";

storiesOf("Video Player", module).add(
  "Player",
  () => {
    const src = text(
      "Video Source",
      "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4"
    );
    const title = text("Video Title", "Video Title");
    return (
      <Center>
        <VideoPlayer src={src} title={title} />
      </Center>
    );
  },
  {
    notes: {
      markdown:
        "# Video Player Breakdown \n Any useful text about the component such as props \n ``` props { text, type:'choice', onClick } ```"
    }
  }
);
