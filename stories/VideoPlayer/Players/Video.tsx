import * as React from "react";
import * as PropTypes from "prop-types";
import injectJss from "react-jss";
const icons = require("../assets/player_icons.png");

const styleSheet = {
  video: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    width: "100%",
    height: "100%",
    backgroundColor: "#000"
  }
};

interface VideoProps {
  src: string;
  title?: string;
  classes?: any;
}

export const test = () => {
  alert("test");
};

const Video: React.FC<VideoProps> = props => {
  React.useEffect(() => {
    console.log("Video Mounted");
  });

  return (
    <video className={props.classes.video} src={props.src} autoPlay muted />
  );
};

Video.propTypes = {
  src: PropTypes.string.isRequired,
  title: PropTypes.string
};

export default injectJss(styleSheet)(Video);
