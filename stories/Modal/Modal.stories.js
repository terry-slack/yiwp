import React from "react";
import { storiesOf, addDecorator } from "@storybook/react";
import { text, boolean, number, select } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import Modal from "./Modal.tsx";
import { StateManager, Center } from "../Helpers";
import { SelectButton } from "../Buttons/index";
import { PhotoPlayer } from "../PhotoPlayer";
import { TextPlayer } from "../TextPlayer";
import { VideoPlayer } from "../VideoPlayer";

storiesOf("Modals", module)
  .add(
    "Modal",
    () => (
      <Modal
        show={true}
        onModalClose={action("Modal Closed")}
        onModalOpen={action("Modal Opened")}
      />
    ),
    {
      notes: {
        markdown:
          "# Button Breakdown \n Any useful text about the component such as props \n ``` props { text, type:'choice', onClick } ```"
      }
    }
  )
  .add(
    "Text",
    () => {
      const contentWidth = number("Content Width");
      const title = text("Title", "Pupin Laboratories");
      const description = text(
        "Description",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."
      );
      return (
        <StateManager initialState={{ show: false }}>
          {({ state, setState }) => (
            <div>
              <Center>
                <SelectButton onClick={() => setState({ show: true })}>
                  Launch modal
                </SelectButton>
              </Center>
              {state.show && (
                <Modal
                  contentWidth={contentWidth}
                  show={state.show}
                  onModalClose={() => {
                    setState({ show: false });
                  }}
                  onModalOpen={action("Modal Open")}
                >
                  <TextPlayer title={title} description={description} />
                </Modal>
              )}
            </div>
          )}
        </StateManager>
      );
    },
    {
      notes: {
        markdown:
          "# Button Breakdown \n Any useful text about the component such as props \n ``` props { text, type:'choice', onClick } ```"
      }
    }
  )
  .add(
    "Photo",
    () => {
      const width = number("Image Width");
      const contentWidth = number("Modal Content Width");
      return (
        <StateManager initialState={{ show: false }}>
          {({ state, setState }) => (
            <div>
              <Center>
                <SelectButton onClick={() => setState({ show: true })}>
                  Launch modal
                </SelectButton>
              </Center>
              {state.show && (
                <Modal
                  contentWidth={contentWidth}
                  show={state.show}
                  onModalClose={() => {
                    setState({ show: false });
                  }}
                  onModalOpen={action("Modal Open")}
                >
                  <PhotoPlayer
                    src={text(
                      "Modal Image Src",
                      "https://images.dailyhive.com/20180807073521/shutterstock_450746794.jpg"
                    )}
                    maxWidth={width}
                  />
                </Modal>
              )}
            </div>
          )}
        </StateManager>
      );
    },
    {
      notes: {
        markdown:
          "# Button Breakdown \n Any useful text about the component such as props \n ``` props { text, type:'choice', onClick } ```"
      }
    }
  )
  .add(
    "Photo & Text",
    () => {
      const width = number("Image Width");
      const contentWidth = number("Modal Content Width");
      const title = text("Title", "Pupin Laboratories");
      const imgSrc = text(
        "Image Source",
        "https://images.dailyhive.com/20180807073521/shutterstock_450746794.jpg"
      );
      const description = text(
        "Description",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."
      );
      return (
        <StateManager initialState={{ show: false }}>
          {({ state, setState }) => (
            <div>
              <Center>
                <SelectButton onClick={() => setState({ show: true })}>
                  Launch modal
                </SelectButton>
              </Center>
              {state.show && (
                <Modal
                  contentWidth={contentWidth}
                  show={state.show}
                  onModalClose={() => {
                    setState({ show: false });
                  }}
                  onModalOpen={action("Modal Open")}
                >
                  <PhotoPlayer src={imgSrc} maxWidth={width} />
                  <TextPlayer title={title} description={description} />
                </Modal>
              )}
            </div>
          )}
        </StateManager>
      );
    },
    {
      notes: {
        markdown:
          "# Button Breakdown \n Any useful text about the component such as props \n ``` props { text, type:'choice', onClick } ```"
      }
    }
  )
  .add(
    "Video",
    () => {
      const contentWidth = number("Modal Content Width");
      const src = text(
        "Video Source",
        "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4"
      );
      const title = text("Video Title", "Video Title");
      return (
        <StateManager initialState={{ show: false }}>
          {({ state, setState }) => (
            <div>
              <Center>
                <SelectButton onClick={() => setState({ show: true })}>
                  Launch modal
                </SelectButton>
              </Center>
              {state.show && (
                <Modal
                  contentWidth={contentWidth}
                  show={state.show}
                  onModalClose={() => {
                    setState({ show: false });
                  }}
                  onModalOpen={action("Modal Open")}
                >
                  <VideoPlayer src={src} title={title} />
                </Modal>
              )}
            </div>
          )}
        </StateManager>
      );
    },
    {
      notes: {
        markdown:
          "# Button Breakdown \n Any useful text about the component such as props \n ``` props { text, type:'choice', onClick } ```"
      }
    }
  );
