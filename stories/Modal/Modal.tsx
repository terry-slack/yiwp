import * as React from "react";
import * as PropTypes from "prop-types";
import injectSheet from "react-jss";
import { TweenMax, Back } from "gsap";
import { string } from "prop-types";

const styleSheet = (theme: any) => ({
  "@import":
    string["url(https://fonts.googleapis.com/css?family=Varela+Round)"],
  hotspotModal: {
    position: "fixed",
    top: "0",
    bottom: "0",
    left: "0",
    transform: "translate3d(0px, 0px, 2px)",
    zIndex: "1",
    color: "white",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    pointerEvents: "all",
    width: "100%",
    height: "100%",
    fontFamily: "Varela Round, sans-serif",
    backgroundColor: "rgba(0,0,0,0.8)",
    opacity: 0
  },
  hotspotModalBackdrop: {
    position: "absolute",
    top: "0",
    bottom: "0",
    left: "0",
    transform: "translate3d(0px, 0px, 2px)",
    pointerEvents: "all",
    width: "100%",
    height: "100%",
    cursor: "pointer",
    zIndex: "1"
  },
  hotspotModalCloseButton: {
    fontSize: "16px",
    fontFamily: "Varela Round, sans-serif",
    position: "absolute",
    verticalAlign: "top",
    right: "16px",
    top: "16px",
    cursor: "pointer",
    zIndex: "500",
    transform: "translate3d(0px, 0px, 2px)",
    zoom: 1,
    pointerEvents: "all",
    width: "36px",
    height: "36px",
    backgroundColor: "rgba(0, 0, 0, 0.4)",
    borderRadius: "50%",
    border: "none",
    color: "#fff",
    "@media (max-width: 900px)": {
      top: "10px",
      right: "10px"
    }
  },
  hotspotModalContent: {
    width: "100%",
    height: "100%",
    padding: "20px",
    boxSizing: "border-box",
    overflowY: "scroll",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexWrap: "wrap",
    zIndex: "3",
    "@media (max-width: 767px)": {
      padding: "0",
      alignItems: "normal",
      width: "100%",
      paddingTop: "100px"
    }
  }
});

interface ModalProps {
  contentWidth?: number;
  show: boolean;
  onModalClose: () => void;
  onModalOpen: () => void;
  classes?: any;
}

const closeModal = (callback: () => void) => {
  TweenMax.to("#modal", 0.35, {
    opacity: 0,
    delay: 0.35,
    onComplete: callback
  });
  TweenMax.to("#modal-content", 0.35, {
    opacity: 0,
    delay: 0,
    y: "+=50",
    ease: Back.easeIn
  });
};

const openModal = (callback: () => void) => {
  TweenMax.to("#modal", 0.5, { opacity: 1 });
  TweenMax.from("#modal-content", 0.5, {
    opacity: 0,
    delay: 0.35,
    y: "+=50",
    ease: Back.easeOut,
    onComplete: callback
  });
};

const Modal: React.FC<ModalProps> = props => {
  console.log(props.contentWidth);
  React.useEffect(() => {
    openModal(props.onModalOpen);
  });
  return (
    <div id="modal" className={props.classes.hotspotModal}>
      <button
        className={props.classes.hotspotModalCloseButton}
        onClick={() => closeModal(props.onModalClose)}
      >
        X
      </button>
      <div
        id="modal-content"
        style={{
          maxWidth: props.contentWidth == null ? "" : `${props.contentWidth}px`
        }}
        className={props.classes.hotspotModalContent}
      >
        <div className={props.classes.hotspotModalContentContainer}>
          {props.children}
        </div>
      </div>
      <div
        className={props.classes.hotspotModalBackdrop}
        onClick={() => closeModal(props.onModalClose)}
      />
    </div>
  );
};

Modal.propTypes = {
  contentWidth: PropTypes.number,
  show: PropTypes.bool.isRequired,
  onModalClose: PropTypes.func.isRequired,
  onModalOpen: PropTypes.func.isRequired
};

Modal.defaultProps = {
  contentWidth: 970,
  show: true,
  onModalClose: Function,
  onModalOpen: Function
};

export default injectSheet(styleSheet)(Modal);
