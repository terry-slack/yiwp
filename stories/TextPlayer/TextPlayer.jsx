import React from "react";
import injectJss from "react-jss";
import compose from "recompose/compose";

const style = {
  player: {
    lineHeight: "22px",
    height: "auto",
    fontSize: "14px",
    color: "#fff",
    padding: "0 30px",
    paddingTop: "30px",
    overflowY: "auto",
    fontWeight: "bold",
    display: "flex",
    flexDirection: "row",
    "@media (max-width: 900px)": {
      flexDirection: "column"
    }
  },
  title: {
    fontSize: "36px",
    lineHeight: "46px",
    marginBottom: "10px",
    flex: "1 1 60%"
  },
  description: {
    lineHeight: "1.5",
    flex: "1 1 40%",
    fontWeight: "300"
  }
};

const TextPlayer = ({ classes, title, description }) => (
  <div className={classes.player}>
    <div
      dangerouslySetInnerHTML={{ __html: title }}
      className={classes.title}
    />
    <span
      className={classes.description}
      dangerouslySetInnerHTML={{ __html: description }}
    />
  </div>
);

export default compose(injectJss(style))(TextPlayer);
