import React from "react";
import injectJss from "react-jss";
import PropTypes from "prop-types";
import compose from "recompose/compose";

const style = {
  imgPreview: {
    maxWidth: ({ maxWidth }) => (maxWidth ? maxWidth + "px" : "100%"),
    maxHeight: ({ maxHeight }) => (maxHeight ? maxHeight + "px" : "initial"),
    display: "flex",
    margin: "auto",
    width: "100%",
    boxShadow: "0px 0px 20px -1px rgba(0,0,0,0.75)"
  }
};

const PhotoPlayer = ({ src, classes }) => (
  <div>
    <img src={src} className={classes.imgPreview} />
  </div>
);

PhotoPlayer.propTypes = {
  src: PropTypes.string,
  maxWidth: PropTypes.number,
  maxHeight: PropTypes.number
};

export default compose(injectJss(style))(PhotoPlayer);
