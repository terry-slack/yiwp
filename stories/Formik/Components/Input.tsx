import * as React from "react";
import injectSheet from "react-jss";
import { string } from "prop-types";

const styleSheet = {
  "@import":
    string["url(https://fonts.googleapis.com/css?family=Varela+Round)"],
  input: {
    "&::placeholder": {
      textOverflow: "ellipsis !important",
      color: "#b7c1e1"
    },
    width: "100%",
    height: "88px",
    objectFit: "contain",
    borderRadius: "6px",
    border: "solid 2px #b7c1e1",
    boxSizing: "border-box",
    padding: "24px 27px",
    fontSize: "32px",
    color: "#4f5b84",
    marginBottom: "14px"
  },
  label: {
    display: "block",
    textAlign: "left",
    paddingLeft: "9px",
    marginBottom: "14px",
    textTransform: "uppercase",
    color: "#223762"
  },
  formGroup: {
    fontFamily: "Varela Round, sans-serif",
    maxWidth: "720px",
    margin: "0 auto"
  }
};

interface Props {
  // classes, placeHolder, onChange, value, name, onBlur
  type: string;
  name: string;
  value: string;
  classes?: any;
  placeHolder?: string;
  onChange: (e: React.ChangeEvent<any>) => void;
  onBlur: (e: React.FocusEvent<any>) => void;
}

const Input: React.FC<Props> = props => {
  return (
    <input
      className={props.classes.input}
      placeholder={props.placeHolder}
      type={props.type}
      name={props.name}
      value={props.value}
      onChange={props.onChange}
      onBlur={props.onBlur}
    />
  );
};

export default injectSheet(styleSheet)(Input);
