import React from "react";
import { storiesOf, addDecorator } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { withA11y } from "@storybook/addon-a11y";
import { text, boolean, number, select } from "@storybook/addon-knobs";
import App from "./Basic.tsx";
import { Center, StateManager } from "../Helpers";

storiesOf("Formik", module).add(
  "Example",
  () => (
    <Center>
      <App />
    </Center>
  ),
  {
    notes: {
      markdown:
        "# Button Breakdown \n Any useful text about the component such as props \n ``` props { text, type:'choice', onClick } ```"
    }
  }
);
