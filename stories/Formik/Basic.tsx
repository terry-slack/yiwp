import * as React from "react";
import injectSheet from "react-jss";
import { withFormik, InjectedFormikProps, FormikProps } from "formik";
import * as Yup from "yup";
import { Input } from "./Components";
import { StepButton } from "../Buttons/index";
import { string } from "prop-types";
import * as image from "./assets/login.png";

const styleSheet = {
  "@import":
    string["url(https://fonts.googleapis.com/css?family=Varela+Round)"],
  label: {
    display: "block",
    textAlign: "left",
    paddingLeft: "9px",
    marginBottom: "14px",
    textTransform: "uppercase",
    color: "#223762",
    fontFamily: "Varela Round, sans-serif",
    fontSize: "18px",
    fontWeight: "bold"
  },
  formWrapper: {
    maxWidth: "790px",
    width: "100%",
    margin: "0 auto"
  },
  heroImage: {
    margin: "0 auto",
    display: "block"
  },
  alignLeft: {
    textAlign: "right"
  },
  error: {
    color: "red",
    fontFamily: "Varela Round, sans-serif",
    paddingLeft: "9px"
  }
};

interface FormValues {
  email: string;
}

interface OtherProps {
  title?: string;
  classes?: any;
}

interface MyFormProps {
  initialEmail?: string;
}

const InnerForm: React.FC<
  InjectedFormikProps<OtherProps, FormValues>
> = props => {
  // const InnerForm = (props: OtherProps & FormikProps<FormValues>) => {

  const {
    classes,
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit,
    isSubmitting,
    title
  } = props;

  console.log(props);

  return (
    <div className={classes.formWrapper}>
      <img className={classes.heroImage} src={image} width={334} />
      <h1>{title}</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <label className={classes.label}>Login</label>
          <Input
            type="email"
            name="email"
            placeHolder="Your Email"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.email}
          />
          {errors && <span className={classes.error}>{errors.email}</span>}
        </div>
        <div className={classes.alignLeft}>
          <StepButton
            type="submit"
            disabled={isSubmitting || !!(errors.email && touched.email)}
          >
            Sign In
          </StepButton>
        </div>
      </form>
    </div>
  );
};

const App = withFormik<MyFormProps, FormValues>({
  mapPropsToValues: props => ({
    email: props.initialEmail || ""
  }),

  validationSchema: Yup.object().shape({
    email: Yup.string()
      .email("Email not valid")
      .required("Email is required")
  }),

  handleSubmit({ email }: FormValues, { props, setSubmitting, setErrors }) {
    console.log(email);
  }
})(injectSheet(styleSheet)(InnerForm));

export default App;
