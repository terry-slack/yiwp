import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { text, boolean, number, select, object } from "@storybook/addon-knobs";
import { Button } from "../containers/Buttons";
import { Center, StateManager } from "../containers/Helpers";

import { options } from "./options";

storiesOf("Application|UI/Buttons", module)
  .addParameters({ jest: ["buttons"] })
  .add("Login", () => (
    <Center>
      <Button onClick={action("clicked")} error={boolean("Error", false)}>
        {text("Text", "Lorem Ipsum")}
      </Button>
    </Center>
  ))
  .add("Logout", () => (
    <Center>
      <Button onClick={action("clicked")} error={boolean("Error", false)}>
        {text("Text", "Lorem Ipsum")}
      </Button>
    </Center>
  ))
  .add("Export", () => (
    <Center>
      <Button onClick={action("clicked")} error={boolean("Error", false)}>
        {text("Text", "Lorem Ipsum")}
      </Button>
    </Center>
  ));

storiesOf("Application|UI/Header", module)
  .addParameters({ jest: ["buttons"] })
  .add("Header", () => (
    <Center>
      <Button onClick={action("clicked")} error={boolean("Error", false)}>
        {text("Text", "Lorem Ipsum")}
      </Button>
    </Center>
  ))
  .add("logo", () => (
    <Center>
      <Button onClick={action("clicked")} error={boolean("Error", false)}>
        {text("Text", "Lorem Ipsum")}
      </Button>
    </Center>
  ))
  .add("User Profile", () => (
    <Center>
      <Button onClick={action("clicked")} error={boolean("Error", false)}>
        {text("Text", "Lorem Ipsum")}
      </Button>
    </Center>
  ));

storiesOf("Application|Pages/Audience", module)
  .addParameters({ jest: ["buttons"] })
  .add("Default", () => (
    <Center>
      <Button onClick={action("clicked")} error={boolean("Error", false)}>
        {text("Text", "Lorem Ipsum")}
      </Button>
    </Center>
  ));

storiesOf("Application|Pages/Login", module)
  .addParameters({ jest: ["buttons"] })
  .add("Default", () => (
    <Center>
      <Button onClick={action("clicked")} error={boolean("Error", false)}>
        {text("Text", "Lorem Ipsum")}
      </Button>
    </Center>
  ));

storiesOf("Application|Tables", module)
  .addParameters({ jest: ["buttons"] })
  .add("List", () => (
    <Center>
      <Button onClick={action("clicked")} error={boolean("Error", false)}>
        {text("Text", "Lorem Ipsum")}
      </Button>
    </Center>
  ))
  .add("Activity", () => (
    <Center>
      <Button onClick={action("clicked")} error={boolean("Error", false)}>
        {text("Text", "Lorem Ipsum")}
      </Button>
    </Center>
  ));
