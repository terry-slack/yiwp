import * as React from "react";
import injectSheet from "react-jss";
import { TweenMax, Back } from "gsap";
import * as PropTypes from "prop-types";

const styleSheet = {
  selectButton: {
    width: "285px",
    height: "80px",
    borderRadius: "7.8px",
    border: "solid 2.6px #085fe5",
    color: "#085fe5",
    fontSize: "20px",
    background: "#fff",
    cursor: "pointer"
  }
};

const hoverState = (event: React.MouseEvent<HTMLButtonElement>) => {
  console.log(event);
  TweenMax.to(event.target, 0.35, {
    color: "#fff",
    backgroundColor: "#085fe5",
    scale: 1.1,
    ease: Back.easeOut
  });
};

const hoverOffState = (event: React.MouseEvent<HTMLButtonElement>) => {
  TweenMax.to(event.target, 0.35, {
    color: "#085fe5",
    backgroundColor: "#fff",
    scale: 1,
    ease: Back.easeOut
  });
};

// More Functional Example
// const Button = ({ onClick, classes, children }: ButtonProps) => {
//   return (
//     <button
//       className={classes.selectButton}
//       onClick={onClick}
//       onMouseEnter={hoverState}
//       onMouseLeave={hoverOffState}
//     >
//       {children}
//     </button>
//   );
// };

interface ButtonProps {
  onClick?: () => void;
  classes: {
    selectButton: string;
  };
}

const Button: React.FC<ButtonProps> = props => {
  // const [text, setText] = React.useState([{ text: "Hooks" }]);

  return (
    <button
      className={props.classes.selectButton}
      onClick={props.onClick}
      onMouseEnter={hoverState}
      onMouseLeave={hoverOffState}
    >
      {props.children}
    </button>
  );
};

Button.propTypes = {
  onClick: PropTypes.func
};

Button.defaultProps = {
  onClick: Function
};

export default injectSheet(styleSheet)(Button);
