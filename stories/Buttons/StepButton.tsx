import * as React from "react";
import injectSheet from "react-jss";
import { TweenMax, Back } from "gsap";
import * as PropTypes from "prop-types";
import { type } from "os";

const styleSheet = {
  stepButton: {
    width: "180px",
    height: "48px",
    borderRadius: "6px",
    backgroundColor: "#085fe5",
    border: "solid 1.6px #085fe5",
    color: "#fff",
    textTransform: "uppercase",
    fontSize: "16px",
    cursor: "pointer"
  }
};

const hoverState = (event: React.MouseEvent<HTMLButtonElement>) => {
  TweenMax.to(event.target, 0.35, {
    color: "#085fe5",
    backgroundColor: "#fff",
    scale: 1.05
  });
};

const hoverOffState = (event: React.MouseEvent<HTMLButtonElement>) => {
  TweenMax.to(event.target, 0.35, {
    color: "#fff",
    backgroundColor: "#085fe5",
    scale: 1
  });
};

// More Functional Example
// const Button = ({ onClick, classes, children }: ButtonProps) => {
//   return (
//     <button
//       className={classes.selectButton}
//       onClick={onClick}
//       onMouseEnter={hoverState}
//       onMouseLeave={hoverOffState}
//     >
//       {children}
//     </button>
//   );
// };

interface ButtonProps {
  onClick?: () => void;
  classes: {
    stepButton: string;
  };
  disabled: boolean;
  type?: "button" | "submit" | "reset" | undefined;
}

const Button: React.FC<ButtonProps> = props => {
  // const [text, setText] = React.useState([{ text: "Hooks" }]);

  return (
    <button
      type={props.type}
      className={props.classes.stepButton}
      onClick={props.onClick}
      onMouseEnter={hoverState}
      onMouseLeave={hoverOffState}
      disabled={props.disabled}
    >
      {props.children}
    </button>
  );
};

Button.propTypes = {
  onClick: PropTypes.func,
  disabled: PropTypes.bool.isRequired
};

Button.defaultProps = {
  onClick: Function
};

export default injectSheet(styleSheet)(Button);
