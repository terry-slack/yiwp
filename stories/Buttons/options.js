let options = {
  data: {
    steps: [
      {
        stepId: -900,
        fields: [
          {
            label: "Are you a...",
            fieldId: "visitortype",
            inputType: "list",
            disabled: false,
            required: true,
            errorMessage: null,
            sortOrder: "-900",
            showBoolean: true,
            defaultValue: null,
            list: [
              {
                value: "prospective_student",
                label: "Prospective Student",
                __typename: "SelectionValueObject"
              },
              {
                value: "parent",
                label: "Parent",
                __typename: "SelectionValueObject"
              },
              {
                value: "other",
                label: "Other",
                __typename: "SelectionValueObject"
              }
            ],
            __typename: "Field"
          }
        ]
      },
      {
        stepId: -900,
        fields: [
          {
            label: "And are you...",
            fieldId: "visitortype",
            inputType: "list",
            disabled: false,
            required: true,
            errorMessage: null,
            sortOrder: "-900",
            showBoolean: true,
            defaultValue: null,
            list: [
              {
                value: "highschool",
                label: "In High School",
                __typename: "SelectionValueObject"
              },
              {
                value: "graduated_highschool",
                label: "Graduated Highschool",
                __typename: "SelectionValueObject"
              },
              {
                value: "college",
                label: "In College",
                __typename: "SelectionValueObject"
              },
              {
                value: "graduated_college",
                label: "Graduated College",
                __typename: "SelectionValueObject"
              },
              {
                value: "adult_learner",
                label: "Adult Learner",
                __typename: "SelectionValueObject"
              },
              {
                value: "middle_school",
                label: "In Middle School",
                __typename: "SelectionValueObject"
              }
            ],
            __typename: "Field"
          }
        ]
      }
    ],
    __typename: "Step"
  }
};

export { options };
