import MobileDetect from "./mobileDetect";
const blackListedDomains = ["urhen", "bcaoo", "11cows", "20minutemail"];
const validate = {
  email: (email: string) => {
    let validEmail = false;
    let validDomain = true;
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      validEmail = true;
    }

    blackListedDomains.forEach((domain, index) => {
      if (email.includes(domain)) validDomain = false;
    });

    if (!validEmail || !validDomain) return true;
    else return false;
  },
  text: (text: string) => {
    if (/^[A-Za-z-'._ ]+$/.test(text)) {
      return false;
    }
    return true;
  },
  phone: (text: string) => {
    if (text.match(/\d/g) == null) return true;
    if (text && text.length > 0) {
      return !(text.match(/\d/g).length >= 10);
    } else return false;
  },
  zipcode: (text: string) => {
    let validZip = /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(text);
    return !validZip;
  },
  postalcode: (text: string) => {
    let validPostal = /([ABCEGHJKLMNPRSTVXY]\d)([ABCEGHJKLMNPRSTVWXYZ]\d){2}/i.test(
      text
    );
    return !validPostal;
  },
  sat: (satScore: string) => {
    let score = parseInt(satScore);
    if (score < 400 || score > 1600) return true;
    else return false;
  },
  act: (actScore: string) => {
    let score = parseInt(actScore);
    if (score < 1 || score > 36) return true;
    else return false;
  },
  gpa: (gpaScore: string) => {
    let scoreToDecimalPlace = parseFloat(gpaScore).toFixed(2);
    let score = parseFloat(scoreToDecimalPlace);
    if (score < 0 || score > 4) return true;
    else return false;
  },
  date: (date: string) => {
    if (!MobileDetect.phone()) {
      // First check for the pattern
      if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(date)) return !false;

      // Parse the date parts to integers
      var parts = date.split("/");
      var day = parseInt(parts[1], 10);
      var month = parseInt(parts[0], 10);
      var year = parseInt(parts[2], 10);
    } else {
      // Parse the mobile date parts to integers
      var parts = date.split("-");
      var day = parseInt(parts[2], 10);
      var month = parseInt(parts[1], 10);
      var year = parseInt(parts[0], 10);
    }

    //get current year
    var currentYear = new Date().getFullYear();
    var age = currentYear - year;

    // Check the ranges of month and age
    if (age < 13 || month == 0 || month > 12) return !false;

    var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    // Adjust for leap years
    if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
      monthLength[1] = 29;

    // Check the range of the day
    return !(day > 0 && day <= monthLength[month - 1]);
  },
  asyncAutocomplete: (autoCompleteValue: string) => {
    // Let asyncAutoComplete pass through in case no item is found
    return false;
  },
  default: (text: string) => {
    return text.length > 0 ? false : true;
  }
};

const validateField = (type: any, value: string, formValues: any) => {
  if (value) {
    switch (type) {
      case "email": {
        return validate.email(value);
        break;
      }
      case "asyncAutocomplete": {
        // Currently used for school inquiry
        return validate.asyncAutocomplete(value);
        break;
      }
      case "phone": {
        return validate.phone(value);
        break;
      }
      case "zip": {
        let country = formValues["country"];

        if (country === "USA") return validate.zipcode(value);
        if (country === "CAN") return validate.postalcode(value);

        return validate.default(value);
        break;
      }
      case "sat": {
        return validate.sat(value);
        break;
      }
      case "act": {
        return validate.act(value);
        break;
      }
      case "gpa": {
        return validate.gpa(value);
        break;
      }
      case "date": {
        return validate.date(value);
        break;
      }
      case "consent_checkbox": {
        // this is gdpr_compliant specific, change type when appropriate
        return !value;
        break;
      }
      default: {
        return validate.default(value);
        break;
      }
    }
  } else {
    return !false;
  }
};

export { validate as default };
export { blackListedDomains, validateField };
