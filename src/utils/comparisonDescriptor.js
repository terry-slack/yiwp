const parseEnrollmentDataString  = (dateString) => {
    let dateArray = dateString.split('_');
    let season = dateArray[1];
    let year = dateArray [0];
    switch(season) {
        case ("spring"):
            return new Date(`01/03/${year}`)
        case ("summer"):
            return new Date(`01/06/${year}`)
        case ("fall"):
            return new Date(`01/09/${year}`)
        case ("winter"):
            return new Date(`01/12/${year}`)            
        default:
            return new Date(`01/09/${year}`)    
    }
}

export default {
    'date': (comparator) => {
        let compFunc;
        switch (comparator) {
            case ">":
                compFunc = (dateA, dateB) => dateA > dateB;
                break;
            case ">=":
                compFunc = (dateA, dateB) => dateA >= dateB;
                break;
            case "<":
                compFunc = (dateA, dateB) => dateA < dateB;
                break;
            case "<=":
                compFunc = (dateA, dateB) => dateA <= dateB;
                break;
            case "=":
                compFunc = (dateA, dateB) => dateA === dateB;
                break;
            default:
                compFunc = (dateA, dateB) => dateA === dateB;
                break;
        }
        return (dateA, dateB)=> {
            // NOTE, this is currently assuming a date format that accords with the the season, year format
            // Needs to be abstracted such that we can handle various types of date formats
            // Not implementing now because there is no relevant use case as we are only using this in
            // a single case, for enrollment/graduation date comparisons
            return compFunc(new Date(`01/06/${dateA}`), parseEnrollmentDataString(dateB.value));
        }

    },
    'number': (comparator) => {
        let compFunc;
        switch (comparator) {
            case ">":
                compFunc = (numberA, numberB) => numberA > numberB;
                break;
            case ">=":
                compFunc = (numberA, numberB) => numberA >= numberB;
                break;
            case "<":
                compFunc = (numberA, numberB) => numberA < numberB;
                break;
            case "<=":
                compFunc = (numberA, numberB) => numberA <= numberB;
                break;
            case "=":
                compFunc = (numberA, numberB) => numberA === numberB;
                break;
            default:
                compFunc = (numberA, numberB) => numberA === numberB;
                break;
        }
        return compFunc
    },
}