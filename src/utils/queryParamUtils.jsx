class QueryParamUtils {
    static useResponsiveUI() {
        try {
            return window.location.href.indexOf('resp=1') != -1
        } catch (e) {
            return false
        }
    }

    static isEmbedded() {
        try {
            return window.location.href.indexOf('embed=1') != -1
        } catch (e) {
            return false
        }
    }


    static isInExpandedState() {
        try {
            return window.location.href.indexOf('hover=1') != -1
        } catch (e) {
            return false
        }
    }

    static isMediaAutoPlayDisabled() {
        try {
            return window.location.href.indexOf('autoplaymedia=0') != -1
        } catch (e) {
            return false
        }
    }
}

export default QueryParamUtils
