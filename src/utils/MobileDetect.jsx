import md from 'mobile-detect'
import UAParser from 'ua-parser-js'

const instance = new md(window.navigator.userAgent)

class MobileDetect {
    static is(identifierString) {
        return instance.is(identifierString)
    }

    static version(identifierString) {
        return instance.version(identifierString)
    }

    static tablet() {
        return instance.tablet()
    }

    static mobile() {
        return instance.mobile()
    }

    static phone() {
        return instance.phone()
    }

    static oculus() {
        const parser = new UAParser()
        const parserResults = parser.getResult(instance.ua)
        return parserResults.browser.name === 'Oculus Browser'
    }

    static userAgent() {
        return instance.userAgent()
    }

    static userAgents() {
        return instance.userAgents()
    }

    static os() {
        return instance.os()
    }
}

export default MobileDetect
