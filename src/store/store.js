import { applyMiddleware, createStore } from "redux";
import reduxThunk from "redux-thunk";
import debounce from "debounce-promise";

import reducer from "./combinedReducers";

export const setupStore = () => {

    const middlewares = [];

    middlewares.push(
        reduxThunk.withExtraArgument({
            debounce
        }),
    );

    return createStore(
        reducer,
        applyMiddleware(...middlewares)
    );
};