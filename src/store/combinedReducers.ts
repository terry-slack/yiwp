//Combine the different reducers
import {combineReducers} from "redux";

import {HomeReducer} from "../containers/Home/reducers/reducers.home";

export default combineReducers({
    home: HomeReducer
});