import React, { FC } from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom"

//Import Containers for Routes
import { Home } from "../Home";
import { PinkFloyd } from "../PinkFloyd";
import { Caribana } from "../Caribana";

interface Props { }

export const
    App: FC<Props> = props => (
        <Router>
            <div className="container">
                <div className="row">
                    <div className="col-sm">
                        <nav className="navbar navbar-dark bg-primary">
                            <Link className="navbar-brand" to="/">NavBar</Link>
                            <Link className="navbar-brand" to="/pinkfloyd?foo=snafu smurf">FooBar</Link>{" "}
                            <Link className="navbar-brand" to="/caribana/Caribana man!!!!!">Caribana</Link>
                        </nav>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">

                        <Switch>
                            <Route exact path="/" component={Home} />
                            <Route exact path="/pinkfloyd" component={PinkFloyd} />
                            <Route exact path="/caribana/:message" component={Caribana} />
                        </Switch>

                    </div>
                </div>
            </div>
        </Router>
    );

