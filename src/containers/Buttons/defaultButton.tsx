import * as React from "react";
import injectSheet from "react-jss";
import { TweenMax, Back } from "gsap";
import * as PropTypes from "prop-types";
import classNames from "classnames";
import MobileDetect from "../../utils/MobileDetect";

const styleSheet = (theme: any) => ({
  button: theme.button.default,
  error: theme.button.error,
  icon: {
    width: "23px",
    height: "23px",
    border: "solid 2px #b7c1e1",
    borderRadius: "100%",
    display: "inline-block",
    float: "left",
    backgroundColor: "rgba(255,255,255,0)"
  },
  arrow: {
    opacity: 0,
    transform: "scale(0)"
  }
});

const hoverState = (
  event:
    | React.MouseEvent<HTMLButtonElement>
    | React.FocusEvent<HTMLButtonElement>
) => {
  event.stopPropagation();
  event.preventDefault();
  TweenMax.to(event.currentTarget, 0.35, {
    color: "#fff",
    backgroundColor: "#085fe5",
    borderColor: "#085fe5",
    boxShadow: "0 4px 10px 0 rgba(8, 95, 229, 0.5)"
  });
};

const hoverOffState = (
  event:
    | React.MouseEvent<HTMLButtonElement>
    | React.FocusEvent<HTMLButtonElement>
) => {
  event.stopPropagation();
  event.preventDefault();
  TweenMax.to(event.currentTarget, 0.35, {
    color: "#085fe5",
    backgroundColor: "#fff",
    borderColor: "#b7c1e1",
    boxShadow: "0 4px 10px 0 rgba(8, 95, 229, 0)"
  });
};

interface ButtonProps {
  onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  classes: any;
  active?: boolean;
  error?: boolean;
  value?: string;
}

const Button: React.FC<ButtonProps> = props => {
  const clickHandler = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    //@ts-ignore// Removes focus off the button once clicked//
    e.target.blur();
    props.onClick ? props.onClick(e) : null;
  };

  return (
    <button
      className={classNames(props.classes.button, {
        [props.classes.error]: props.error
      })}
      onFocus={props.active ? undefined : hoverState}
      onBlur={props.active ? undefined : hoverOffState}
      value={props.value}
      onClick={clickHandler}
      onMouseEnter={
        MobileDetect.tablet() || MobileDetect.mobile() ? undefined : hoverState
      }
      onMouseLeave={
        MobileDetect.tablet() || MobileDetect.mobile()
          ? undefined
          : hoverOffState
      }
    >
      {props.children}
    </button>
  );
};

Button.propTypes = {
  onClick: PropTypes.func,
  active: PropTypes.bool,
  error: PropTypes.bool
};

Button.defaultProps = {
  active: false,
  error: false
};

export default injectSheet(styleSheet)(Button);
