//This is the entry point for the home component
import React, { FC } from "react";
import { useParams } from 'react-router-dom'
import queryString from 'query-string'
import { connect } from 'react-redux';
import injectSheet from "react-jss";

import { StyleSheet } from "./index.css";
import { Aux } from "../../hoc/Auxiliary";
import { Label } from "../../shared/components/Label";
export const
    _Caribana: FC<any> = props => (
        <Aux>
            <Label>{props.match.params.message}</Label>
            <i className={`fas fa-smile ${props.classes.smiley}`}></i>
            <img
                alt="Toronto Caribbean Carnival"
                src="https://upload.wikimedia.org/wikipedia/commons/5/5e/TORONTO_CARIBBEAN_CARNIVAL_-LOGO.jpg"
            />
        </Aux>
    );

export const
    Caribana = connect(null)(injectSheet(StyleSheet)(_Caribana));