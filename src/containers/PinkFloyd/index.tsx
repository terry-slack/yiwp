//This is the entry point for the home component
import React,{FC} from "react";
import queryString from 'query-string'
import { connect } from 'react-redux';
import injectSheet from "react-jss";

import { Label } from "../../shared/components/Label";
import { Aux } from "../../hoc/Auxiliary";
import { StyleSheet } from "./index.css";
export const
    _PinkFloyd:FC<any> = props =>{        
        const 
            foo = queryString.parse(location.search);        
        return(
        <Aux>
            <Label>Da Band</Label>
            <br />
            <Label>and from the query string parameter called foo we have the value </Label>
            <br />
            <Label>{foo.foo}</Label>
            <img
                alt="Toronto Caribbean Carnival"
                src="https://i.ytimg.com/vi/-EzURpTF5c8/maxresdefault.jpg"
                className={props.classes.roundImage}
            />
        </Aux>
    )};
export const
    PinkFloyd = connect(null)(injectSheet(StyleSheet)(_PinkFloyd));