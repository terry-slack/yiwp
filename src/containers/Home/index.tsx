//This is the entry point for the home component
import React, { useEffect, FC } from "react";
import { useSelector, useDispatch } from 'react-redux';

import { Actions } from "./actions/actions.home";
import { IHome } from "./reducers/reducers.home";
import { Ulist } from "../../shared/components/Ulist";

const
    countrylistEffect = (dispatch:any, countriesLength:number) => 
        () => { countriesLength === 0 && dispatch(Actions.northAmericanCountryList()) }

export const 
    Home:FC<any> = props => {
        const
            dispatch = useDispatch(),
            home:IHome = useSelector(({ home }:{home:IHome}) => home);
        /*
            How do I memoize the component?  Or can I

            The other two routes are perfect for memoization

            Can I look at preloading the images
            https://medium.com/jsguru/react-image-lazy-loading-component-246e0cdcce02
        */    
        useEffect(
            countrylistEffect(dispatch, home.northAmericanCountries.length)
        );
        return (<Ulist labelValue={home.listLabelValue} list={home.northAmericanCountries} />)
    };