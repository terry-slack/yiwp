//Constants for for actions
export const
    HOME_LIST_EXISTS = "HOME_LIST_EXISTS",
    HOME_ERROR = "HOME_ERROR",
    HOME_START = "HOME_START",
    HOME_SUCCESS = "HOME_SUCCESS";    

export interface INorthAmericanCountries{
    value:string
}

const //This is only used as part of the fake api call to get the country list
    northAmericanCountries:INorthAmericanCountries[] = [
        {
            value: "Canada"
        },
        {
            value: "America"
        },
        {
            value: "Mexico"
        }
    ],
    northAmericanCountryList = () => async (dispatch:any) => {
        /*
          Fake an api call, using an asnyc action creator
        */
        console.log(`in action`);

        const
            request = (payload:any) => ({ type: HOME_SUCCESS, payload }),
            error = (payload:any) => ({ type: HOME_ERROR, payload });

        console.log(`Countries fetched:`);
        //Reset the state supporting the autocomplete
        await dispatch(request(northAmericanCountries));
    };
export const Actions = {
    northAmericanCountryList
};