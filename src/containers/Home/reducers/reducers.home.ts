import * as actionTypes from "../actions/actions.home";
import { ActionObject } from "shared/interfaces/interfaces";

interface InitialState {
    northAmericanCountries:string[],
    listLabelValue:string,
    error: string
}

export interface IHome extends InitialState {}
const initialState:InitialState = {
    northAmericanCountries:[],
    listLabelValue:"North American Countries",
    error: ""
};

export const HomeReducer = (state = initialState, action:ActionObject) => {
    switch (action.type) {
        case actionTypes.HOME_ERROR:
            //Update the error in state
            return {
                ...state,
                error: action.payload
            };

        case actionTypes.HOME_START:
            //Just a default action.  We could have a follow through to default, but
            //Better to leave it grouped with the Home actions, in case things change in the future.
            return state;

        case actionTypes.HOME_SUCCESS:
            //Use destructuring to create an array of North American countries.  This 
            //Also takes into account any new ones added            
            return {
                ...state,
                northAmericanCountries: [...state.northAmericanCountries, ...action.payload]
            };

        default:
            return state;
    }
};