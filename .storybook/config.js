import { configure, addParameters, addDecorator } from "@storybook/react";
import { create } from "@storybook/theming";
import { INITIAL_VIEWPORTS } from "@storybook/addon-viewport";
import { withTests } from "@storybook/addon-jest";
import { withA11y } from "@storybook/addon-a11y";
import { withKnobs } from "@storybook/addon-knobs";
import { withThemesProvider } from "storybook-addon-jss-theme";
import { storybookTheme } from "./../themes";
import { DocsPage, DocsContainer } from "@storybook/addon-docs/blocks";
import { action } from "@storybook/addon-actions";
import results from "../stories/.jest-test-results.json";

addDecorator(withThemesProvider([storybookTheme]));
addParameters({
  docs: {
    container: DocsContainer,
    page: DocsPage
  }
});
addDecorator(withA11y);
addDecorator(withKnobs);
addDecorator(
  withTests({
    results
  })
);
addParameters({
  options: {
    theme: create({
      base: "light",
      brandTitle: "YIWP",
      brandUrl: "https://bitbucket.org/youvisit/registration-app/",
      gridCellSize: 12
    }),
    hierarchySeparator: /\//,
    hierarchyRootSeparator: /\|/
  },
  viewport: {
    viewports: {
      ...INITIAL_VIEWPORTS
    }
  }
});

// automatically import all files ending in *.stories.js
require("../stories/index.js");
configure(require.context("../stories", true, /\.stories\.js$/), module);
