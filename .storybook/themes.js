const defaultTheme = {
  name: "DEFAULT",
  variables: {
    center: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      height: "100%",
      position: "fixed",
      width: "100%",
      left: 0,
      top: 0
    }
  }
};

const darkTheme = {
  name: "DARK",
  variables: {
    backgroundColor: "black",
    textColor: "white",
    borderRadius: "100px"
  }
};

export default [defaultTheme];
