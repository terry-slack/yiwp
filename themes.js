const theme = {
  center: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
    position: "fixed",
    width: "100%",
    left: 0,
    top: 0,
    flexWrap: "wrap",
    alignContent: "center"
  },
  button: {
    default: {
      backgroundColor: "#fff",
      outline: 0,
      boxShadow: "0 4px 10px 0 rgba(8, 95, 229, 0)",
      fontSize: "18px",
      fontWeight: "500",
      width: "242px",
      height: "72px",
      borderRadius: "6px",
      border: "solid 2px #b7c1e1",
      color: "#085fe5",
      cursor: "pointer",
      // padding: "0 24px",
      lineHeight: "25px",
      "@media (max-width: 900px)": {
        fontWeight: "bold"
      }
    },
    error: {
      borderColor: "#e60000 !important",
      transition: "border 500ms"
    }
  },
  error: {
    textAlign: "left",
    paddingLeft: "32px",
    marginTop: "12px",
    fontSize: "12px",
    fontWeight: "500",
    textTransform: "uppercase",
    color: "#d0021b !important"
  },
  formWrapper: {
    width: "100%",
    margin: "0 auto",
    position: "relative",
    maxWidth: "820px",
    paddingTop: "60px",
    "@media (max-width: 900px)": {
      paddingTop: "35px",
      "& > div": {
        padding: "0 !important"
      },
      padding: "0 38px",
      boxSizing: "border-box"
    }
  }
};

export const storybookTheme = {
  name: "DEFAULT",
  variables: theme
};

const darkTheme = {
  name: "DARK",
  variables: {
    backgroundColor: "black",
    textColor: "white",
    borderRadius: "100px"
  }
};

export default theme;
